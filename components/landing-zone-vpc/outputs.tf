
output "vpc_id" {
  description = "VPC ID"
  value             =         aws_vpc.vpc_main.id
}

output "internet_gateway_id" {
  description = "Internet Gateway id"
  value             =         aws_internet_gateway.gw.id
}
output "subnets_publicids" {
  value             =         aws_subnet.public_subnet.*.id
}
output "subnets_privateids" {
  value             =         aws_subnet.private_subnet.*.id
}
output "private_subnet_id1" {
   value            =    element(aws_subnet.private_subnet.*.id,0) # 0->subnet 1a, 1->subnet 1b and so on
}
output "private_subnet_id2" {
   value            =    element(aws_subnet.private_subnet.*.id,0) # 0->subnet 1a, 1->subnet 1b and so on
}

output "public_subnet_id1" {
   value            =    element(aws_subnet.public_subnet.*.id,0) # 0->subnet 1a, 1->subnet 1b and so on
}
output "public_subnet_id2" {
   value            =    element(aws_subnet.public_subnet.*.id,1) # 0->subnet 1a, 1->subnet 1b and so on
}

output "public_route_table_id" {
   value            =   aws_route_table.public_rt.id
}

output "Key-pair"{
   value            =    aws_key_pair.App_instance_key.key_name
}