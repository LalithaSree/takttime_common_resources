
#VPC
resource "aws_vpc" "vpc_main" {
  //cidr block for vpc
  cidr_block          =   var.vpc_cidr_block
  //makes your instances shared on host
  instance_tenancy    =   "default"
  //enable/disable dns hostnames
  enable_dns_hostnames=   true
  
  tags = {
    Name              = "${var.env}_vpc"
    Environment       =   var.env
  }
}

resource "aws_internet_gateway" "gw" {
  //VPC id to create gateway in
  vpc_id              =    aws_vpc.vpc_main.id
  // A map of tags to assign to the resource
  
  tags={
    Name              =    "${var.env}_gw"
    Environment       =     var.env
  }

  lifecycle {
    create_before_destroy =   true
  }
}

resource  "aws_eip" "nat"{
  //EIP may require IGW to exist prior to association
  //Use depends_on to set the explicit dependency on IGW
  depends_on           =    [aws_internet_gateway.gw]
  tags = {
    Name               =    "eip"
    Environment        =    var.env
  }
}


#CREATING A NAT GATEWAY TO PROVIDE INTERNET ACCESS TO MY private EC2S

resource "aws_nat_gateway" "nat_gw" {
  
  //The ALLOCATION ID of the ELASTIC IP address for the gateway
  allocation_id         =     aws_eip.nat.id
  //The Subnet ID of the subnet in which to place the gateway
  subnet_id             =     element(aws_subnet.public_subnet.*.id,0)
  //A map of tags to assign to resources
  tags = {
    Name                =     "${var.env}_nat_gw"
    Environment         =     var.env
  }
}



#PUBLIC SUBNETS
resource "aws_subnet" "public_subnet"{

  count                 =    length(var.public_subnets) #length(data.aws_availability_zones.azs.names)

  vpc_id                =    aws_vpc.vpc_main.id
  cidr_block            =    element(var.public_subnets,count.index) #For each availability zone, creating a subnet using element, for now hard coded it to 1 subnet for each az
  availability_zone     =    var.availability_zones[count.index]

  tags = {
    Name                =    "${var.env}_public_subnet_${var.availability_zones[count.index]}"
    Environment         =    var.env
  }

}

#PRIVATE SUBNETS
resource "aws_subnet" "private_subnet"{
  count                 =    length(var.private_subnets) #length(data.aws_availability_zones.azs.names)

  vpc_id                =    aws_vpc.vpc_main.id
  cidr_block            =    element(var.private_subnets,count.index) #For each availability zone, creating a subnet using element
  availability_zone     =     var.availability_zones[count.index]

  tags = {
    Name                =    "${var.env}_private_subnet_${var.availability_zones[count.index]}"
    Environment         =    var.env
  }

}

/*
resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id     =     var.instance_sg_id
  network_interface_id  =     var.network_interface_id
}
*/
