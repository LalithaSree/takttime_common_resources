###########################################################################
#                         NETWORK VARIABLES                               #
###########################################################################


variable "vpc_cidr_block"{
    type            =          string
}
variable "availability_zones" {
  description       = "Availability zones in this region to use"
  type              = list(string)
}

variable "public_subnets"{
    description     =       "Subnet CIDRs for public subnets"
    type            =       list(string)
    
}

variable "private_subnets"{
    description     =       "Subnet CIDRs for private subnets"
    type            =       list(string)
   
}

variable "env" {
      type          =  string
}

variable "key_name"{
    description     = "key name to launch instances"
    type            =  string
}




/*
variable "enable_nat_gateway"{
    description     = "enable nat gateway? boolean value"
    type            =  bool
}
/*
variable "instance_sg_id"{
    description     = "SG id for instance"
    type            =  string
}

variable "network_interface_id" {
    description     =   "primary network interface id"
    type            =  string
}
*/
/*variable "region"{
    type           =        string
}*/
