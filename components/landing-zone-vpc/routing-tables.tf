
#AWS PUBLIC ROUTE-TABLE
resource "aws_route_table" "public_rt"{
  vpc_id            =    aws_vpc.vpc_main.id
  
  route {
      //The CIDR block of the route
      cidr_block    =   "0.0.0.0/0"
      //direct the traffic to internet gateway
      gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name            =    "Public-route-table"
    Environment     =    var.env
  }
}
#ASSOCIATE AWS PUBLIC ROUTE-TABLE TO PUBLIC SUBNETS
resource "aws_route_table_association" "public_rta"{
  count             =     length(var.public_subnets)
  subnet_id         =     element(aws_subnet.public_subnet.*.id,count.index)
  route_table_id    =     aws_route_table.public_rt.id
}



#AWS PRIVATE ROUTE-TABLE
resource "aws_route_table" "private_rt"{
  vpc_id            =    aws_vpc.vpc_main.id
  
  route {
      //The CIDR block of the route
      cidr_block    =   "0.0.0.0/0"
      //direct the traffic to NAT gateway
      gateway_id = aws_nat_gateway.nat_gw.id  
  }
  tags = {
    Name            =    "Private-route-table"
    Environment     =    var.env
  }
}




#ASSOCIATE AWS PRIVATE ROUTE-TABLE TO PRIVATE SUBNETS
resource "aws_route_table_association" "private_rta"{
  count             =     length(var.private_subnets)    
  subnet_id         =     element(aws_subnet.private_subnet.*.id,count.index)
  route_table_id    =     aws_route_table.private_rt.id                    
}
