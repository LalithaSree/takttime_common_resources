
variable "dynamodb_lock_table_name" {
  type = string
  default = "terraform-lock"
}
variable "backend_bucket" {
  type = string
  default = "terraform-lock"
}