/*
 * Module: terraform-aws-backend
 *
 * Bootstrap your terraform backend on AWS.
 * This module configures resources for state locking for terraform >= 0.14.7 


 * This template creates and/or manages the following resources
 *   - An S3 Bucket for storing terraform state
 *   - An S3 Bucket for storing logs from the state bucket
*/


 data "aws_caller_identity" "current" {
}

resource "aws_s3_bucket" "tf_backend_bucket"{
    bucket = var.backend_bucket
     acl    = "private"
     versioning {
     enabled = false    //using false for now
      }

       tags = {
    Description        = "Terraform S3 Backend bucket which stores the terraform state for account ${data.aws_caller_identity.current.account_id}."
    ManagedByTerraform = "true"
    TerraformModule    = "terraform-aws-backend"
  }
}


resource "aws_dynamodb_table" "tf_backend_state_lock_table" {
  name             = var.dynamodb_lock_table_name
  hash_key         = "LockID"
  
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Description        = "Terraform state locking table for account ${data.aws_caller_identity.current.account_id}."
    ManagedByTerraform = "true"
    TerraformModule    = "terraform-aws-backend"
  }
  lifecycle {
    prevent_destroy = true
  }
}
