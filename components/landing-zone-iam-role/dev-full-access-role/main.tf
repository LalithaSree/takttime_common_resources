#********************************************************************************/
#             Developer full access role
#*******************************************************************************
//Creating a role

resource "aws_iam_role" "dev-role" {
  name = var.rolename
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Action    = "sts:AssumeRole",
        Principal = { "AWS" : "arn:aws:iam::745172767028:root" } //role for identity account
    }]
  })
 # provider = aws.dev
}

//giving power user permissions to developers
 resource "aws_iam_policy_attachment" "poweruser" {
  count = var.isdev_fullaccessrole ? 1 : 0

  name       = "list s3 buckets policy to role"
  roles      = [aws_iam_role.dev-role.name]
  policy_arn = "arn:aws:iam::aws:policy/PowerUserAccess" 
  #provider   = aws.prod
}

//attach the s3limitedaccess policy to the role in development
 resource "aws_iam_policy_attachment" "s3-limited" {
  count = var.isdev_fullaccessrole ? 0 : 1

  name       = "list s3 buckets policy to role"
  roles      = [aws_iam_role.dev-role.name]
  policy_arn = aws_iam_policy.s3limitedaccess.arn  
  #provider   = aws.prod
}

//attach the ec2restrict family access policy to the role in development
 resource "aws_iam_policy_attachment" "ec2-limited" {
  count = var.isdev_fullaccessrole ? 0 : 1

  name       = "ec2limitedaccess policy to role"
  roles      = [aws_iam_role.dev-role.name]
  policy_arn = aws_iam_policy.ec2restrictfamily.arn  
  #provider   = aws.prod
}


/*
 resource "aws_iam_policy_attachment" "dbtableaccess" {
  name       = "dbtableaccess policy to role"
  roles      = [aws_iam_role.this.name]
  policy_arn = aws_iam_policy.dbtableaccess.arn
  #provider   = aws.prod
}
 resource "aws_iam_policy_attachment" "VPCfullaccess" {
  name       = "VPCfullaccess policy to role"
  roles      = [aws_iam_role.this.name]
  policy_arn = aws_iam_policy.VPCfullaccess.arn
  #provider   = aws.prod
}

 resource "aws_iam_policy_attachment" "AWSLambda_FullAccess" {
  name       = "AWSLambda_FullAccess policy to role"
  roles      = [aws_iam_role.this.name]
  policy_arn = aws_iam_policy.AWSLambda_FullAccess.arn
  #provider   = aws.prod
}

*/