
# deny deleting buckets and objects

resource "aws_iam_policy" "s3limitedaccess" {
  name = "s3limitedaccess"
  policy = jsonencode({
  "Id": "Policy1630521437937",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1630521411901",
      "Action": [
        "s3:DeleteBucket",
        "s3:DeleteObject"
      ],
      "Effect": "Deny",
      "Resource": "*" 
    },
    {
      "Sid": "Stmt1630521428617",
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
})
}

resource "aws_iam_policy" "ec2restrictfamily" {
    name = "ec2restrictfamily"
    policy = jsonencode({
    "Id": "Policy1630521437937",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "ec2:*",
            "Resource": "*",
            "Condition":{
                "ForAllValues:StringLike":{
                    "ec2:InstanceType":[
                      "t3.nano",
                       "a1.medium",
                       "m5.large"
                    ]
                }
            }
        }
    ]
})
}
