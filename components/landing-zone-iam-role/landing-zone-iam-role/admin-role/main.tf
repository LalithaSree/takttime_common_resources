//Creating a role

resource "aws_iam_role" "this" {
  name = var.rolename
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Action    = "sts:AssumeRole",
        Principal = { "AWS" : "arn:aws:iam::745172767028:root" } //role for identity account
    }]
  })
}
/*
resource "aws_iam_policy" "adminAccess_policy" {
  name = "AdministratorAccess"
  policy = jsonencode({
    
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": "*",
                "Resource": "*"
            }
        ]
    
  })
}
*/
 resource "aws_iam_policy_attachment" "adminaccess" {
  name       = "admin access to this role"
  roles      = [aws_iam_role.this.name]
  policy_arn =  "arn:aws:iam::aws:policy/AdministratorAccess" //aws_iam_policy.adminAccess_policy.arn
}
